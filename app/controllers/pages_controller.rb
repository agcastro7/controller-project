class PagesController < ApplicationController

  def index
    @pages = Page.all
  end

  def show
    @page = Page.find(1)
  end

  def new
    @page = Page.new
  end

  def edit
  end

  def create
    @page = Page.new(params.require(:page).permit(:title, :subtitle, :author, :content))
    @page.save
  end

  def update
    @page = Page.find(params[:id])
    @page.update(params.require(:page).permit(:title, :subtitle, :author, :content))
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
  end

  def like
    @page = Page.find(params[:id])
  end

  def dislike
    @page = Page.find(params[:id])
  end

  def authors
    @authors = Page.distinct.pluck(:author)
    @authors.delete('Unknown')
  end

  def delete_author
    author = params[:name]
    pages = Page.where(author: author)
    pages.each do |page|
      page.update!(author: 'Unknown')
    end
    redirect_to authors_path, notice: "#{author} posts were set to Unknown"
  end
end
