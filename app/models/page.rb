# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  title      :string
#  subtitle   :string
#  content    :text
#  likes      :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Page < ApplicationRecord
  validates :title, presence: true
  validates :subtitle, presence: true
  validates :content, presence: true

  def publication_date
    created_at.strftime('%D')
  end

  def like
    self.likes += 1
  end

  def dislike
    unless self.likes == 0
      self.likes -= 1
    end
  end
end
