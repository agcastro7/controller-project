Rails.application.routes.draw do
  root to: 'pages#index'

  resources :pages do
    put '/like', to: 'pages#like', on: :member
    put '/dislike', to: 'pages#dislike', on: :member
  end

  get 'authors', to: 'pages#authors', as: :authors
  delete 'author/:name', to: 'pages#delete_author', as: :delete_author
end
