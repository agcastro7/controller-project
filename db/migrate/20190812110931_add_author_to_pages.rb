class AddAuthorToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :author, :string
  end
end
