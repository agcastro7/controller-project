class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :subtitle
      t.text :content
      t.integer :likes

      t.timestamps
    end
  end
end
