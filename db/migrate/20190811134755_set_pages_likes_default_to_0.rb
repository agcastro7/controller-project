class SetPagesLikesDefaultTo0 < ActiveRecord::Migration[5.2]
  def change
    change_column_default(
      :pages,
      :likes,
      from: nil,
      to: 0
    )
  end
end
