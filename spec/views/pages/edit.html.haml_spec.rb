require 'rails_helper'

RSpec.describe "pages/edit", type: :view do
  before(:each) do
    @page = assign(:page, Page.create!(
      :title => "MyString",
      :subtitle => "MyString",
      :content => "MyText",
      :likes => 1
    ))
  end

  it "renders the edit page form" do
    render

    assert_select "form[action=?][method=?]", page_path(@page), "post" do

      assert_select "input[name=?]", "page[title]"

      assert_select "input[name=?]", "page[subtitle]"

      assert_select "textarea[name=?]", "page[content]"

      assert_select "input[name=?]", "page[likes]"
    end
  end
end
