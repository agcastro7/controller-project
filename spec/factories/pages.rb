# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  title      :string
#  subtitle   :string
#  content    :text
#  likes      :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :page do
    title { "MyString" }
    subtitle { "MyString" }
    content { "MyText" }
    likes { 1 }
  end
end
